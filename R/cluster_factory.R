library(R6)


cluster_factory <- R6Class(

  "Cluster",

  private = list(

    ..dist_matrix = "a",
    ..cluster = "b",
    ..clustering_method = "c",
    ..cluster_object = "d",
    ..metric_method = "e"


  ),

  public = list(

    initialize = function(dist_matrix, metric_method, clustering_method, cluster_vector, cluster_object) {

      private$..dist_matrix <- dist_matrix
      private$..cluster <- cluster_vector

      if (!missing(clustering_method)) {

        private$..clustering_method <- clustering_method

      }

      if (!missing(metric_method)) {

        private$..metric_method <- metric_method

      }

      if (!missing(cluster_object)) {
        ## Guardo el objeto de la librería que genera el cluster para poder aprovechar sus características específicas,
        ## como la representación del dendograma en hclust
        private$..cluster_object <- cluster_object
      }

    }

  ),

  active = list(

    clustering_method = function() {

      private$..clustering_method

    },

    metric = function(value) {

      if (!missing(value)) {

        private$..metric_method <- value

      } else {

        private$..metric_method

      }
    },

    cluster = function() {

      private$..cluster

    },

    distance_matrix = function() {

      private$..dist_matrix
    }
  )
)

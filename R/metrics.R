## LOCK-STEP METRICS


#' Calculate the minkowski distance matrix for a group of timeseries
#'
#' This function calculates the minkowski distance matrix for the given timeseries. First of all, the
#' timeseries are cleaned and normalized with 'clean_ts' function, removing NAs, or entire timeseries if the NA percentage
#' exceeds the na_percentage_to_input value.
#'
#' @param timeseries The group of timeseries as an xts object.
#' @param order The order of the minkowski metric.
#' @param ...
#'
#' @return A distance matrix for the group of timeseries using the minkowski metric. It is important to note that the timeseries
#' are first cleaned and normalized with the 'clean_ts' function.
#'
#' @export
#'
#' @examples
#' data(sunspots)
#' ts <- xts::as.xts(sunspots)
#' minkowski(ts)
#' minkowski(ts, order = 4)
minkowski <- function (timeseries, order = 2, ...) {

  dist(t(timeseries), "minkowski", p = order, ...)

}


#' Calculate the correlation distance matrix for a group of timeseries
#'
#' This function calculates the correlation distance matrix for the given timeseries.
#' We use linear correlation coeficient as distance measure. First of all, the timeseries are
#' cleaned and normalized with 'clean_ts function, removing NAs, or entire timeseries if
#' the NA percentage exceeds the na_percentage_to_input value.
#'
#' @param timeseries The group of timeseries as an xts object.
#' @param na_percentage_to_input Maximum percentage of NAs that a timeseries can have before it is discarted.
#' @param na_input_method  Method of interpolation to input NAs in a timeseries. It should be
#' one of "linear", "spline", "locf" or "nocb".
#' @param interpolate Boolean that indicates if the timeseries should interpolate other points in the time domain,
#' given that the domains of the timeseries are not identical.
#' @param ...
#'
#' @return A distance matrix for the group of timeseries using the linear correlation coeficient as metric. It is important to note that the timeseries
#' are first cleaned and normalized with the 'clean_ts' function.
#' @export
#'
#' @examples
#' correlation_distance(timeseries_samples)
correlation_distance <- function (timeseries, ...) {

  as.dist(1 - cor(timeseries, ...))

}


## ELASTIC METRICS


#' Calculate the dynamic time warping distance matrix for a group of timeseries
#'
#'
#' @param timeseries The group of timeseries as an xts object.
#'
#' @return A distance matrix for the group of timeseries using the dynamic time warping metric. It is important to note that the timeseries
#' are first cleaned and normalized with the 'clean_ts' function.
#' @export
#'
#' @examples
#' dynamic_time_warping(timeseries_samples)
dynamic_time_warping <- function (timeseries) {

  as.dist(dtw::dtwDist(t(timeseries)))

}


#' Calculate the longest common subsequence distance matrix for a group of timeseries
#'
#' @param timeseries The group of timeseries as an xts object.
#' @param epsilon
#' @param na_percentage_to_input
#' @param na_input_method
#' @param ...
#'
#' @return
#' @export
#'
#' @examples
lcss <- function(timeseries, epsilon, na_percentage_to_input = 0.5,
                 na_input_method = c("linear", "spline", "locf", "nocb"), ...){

  na_input_method <- match.arg(na_input_method)

  timeseries <- clean_ts(timeseries, na_percentage_to_input, na_input_method, interpolate = FALSE)

  TSdist::TSDatabaseDistances(timeseries, distance="lcss", epsilon=epsilon, ...)

}


### FEATURE-BASED METRICS


#' Calculate the discrete fourier transform distance matrix for a group of timeseries
#'
#' @param timeseries_list The group of timeseries as an xts object.
#' @param order
#' @param na_percentage_to_input
#' @param na_input_method
#' @param dft_number_of_points
#'
#' @return
#' @export
#'
#' @examples
discrete_fourier_transform_distance <- function (timeseries_list, order = 2, na_percentage_to_input = 0.5,
                                        na_input_method = c("linear", "spline", "locf", "nocb"),
                                        dft_number_of_points = 10) {

  na_input_method <- match.arg(na_input_method)

  timeseries <- clean_ts(timeseries, na_percentage_to_input, na_input_method, interpolate = FALSE)

  dft_matrix <- apply(timeseries, 2, fft)

  n <- dim(dft_matrix)[1]
  m <- dim(dft_matrix)[2]

  # Al calcular la matriz de distancias con dist, podemos emplear más métricas que la euclídea

  if (dft_number_of_points > m) {

    dist(t(dft_matrix[1:dft_number_of_points,]))

  } else {

    dist(t(dft_matrix))

  }

}


#' Calculate the minkowski distance matrix for a group of timeseries
#'
#' @param timeseries The group of timeseries as an xts object.
#' @param n_windows
#' @param na_percentage_to_input
#' @param na_input_method
#' @param ...
#'
#' @return
#' @export
#'
#' @examples
sax <- function(timeseries, n_windows, na_percentage_to_input = 0.5,
                na_input_method = c("linear", "spline", "locf", "nocb"), ...){
  # Mismo problema que DFT. No es optima la implementacion en los paquetes existentes.

  na_input_method <- match.arg(na_input_method)

  timeseries <- clean_ts(timeseries, na_percentage_to_input, na_input_method, interpolate)

  TSdist::TSDatabaseDistances(timeseries, distance="mindist.sax", w = n_windows)
}


#' Title
#'
#' @param timeseries The group of timeseries as an xts object.
#' @param na_percentage_to_input
#' @param na_input_method
#' @param ...
#'
#' @return
#' @export
#'
#' @examples
dwt <- function(timeseries, na_percentage_to_input = 0.5,
                na_input_method = c("linear", "spline", "locf", "nocb"), ...){
  # Introducir la posibilidad de elegir otras funciones madre y padre. Y que funcione.
  na_input_method <- match.arg(na_input_method)

  timeseries <- clean_ts(timeseries, na_percentage_to_input, na_input_method, interpolate)

  TSdist::TSDatabaseDistances(timeseries, distance = "wav")

}
